from django.apps import AppConfig


class ParseroConfig(AppConfig):
    name = 'parsero'
